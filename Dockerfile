FROM ubuntu
RUN apt-get update && apt-get install -y \
  cmake
RUN mkdir dlib-example/
COPY ./* dlib-example/
RUN cd ./dlib-testbed/ && \
  mkdir build && \
  cd build && \
  cmake .. && \
  cmake --build .
CMD [./src/main]
