#include "dlib/bayes_utils.h"
#include "dlib/graph_utils.h"
#include "dlib/graph.h"
#include "dlib/directed_graph.h"
#include <iostream>

int main()
try
{
  using namespace std;
  using namespace dlib;
  using namespace bayes_node_utils;

  directed_graph<bayes_node>::kernel_1a_c bn;

  enum nodes
  {
    Cloudy    = 0,
    Sprinkler = 1,
    Rain      = 2,
    Wet       = 3
  };
  
  bn.set_number_of_nodes(4);
  bn.add_edge(Sprinkler, Wet);
  bn.add_edge(Rain, Wet);
  bn.add_edge(Cloudy, Sprinkler);
  bn.add_edge(Cloudy, Rain);

  set_node_num_values(bn, Cloudy   , 2);
  set_node_num_values(bn, Sprinkler, 2);
  set_node_num_values(bn, Rain     , 2);
  set_node_num_values(bn, Wet      , 2);

  assignment parent_state;
  
  // Cloudy node
  set_node_probability(bn, Cloudy, 0, parent_state, 0.5);
  set_node_probability(bn, Cloudy, 1, parent_state, 1 - 0.5);

  // Sprinkler State
  parent_state.add(Cloudy, 1);

  parent_state[Cloudy] = 0;
  set_node_probability(bn, Sprinkler, 0, parent_state, 0.5);
  set_node_probability(bn, Sprinkler, 1, parent_state, 1 - 0.5);

  parent_state[Cloudy] = 1;
  set_node_probability(bn, Sprinkler, 0, parent_state, 0.9);
  set_node_probability(bn, Sprinkler, 1, parent_state, 1 - 0.9);

  // Rain node
  parent_state[Cloudy] = 0;
  set_node_probability(bn, Rain, 0, parent_state, 0.8);
  set_node_probability(bn, Rain, 1, parent_state, 1 - 0.8);

  parent_state[Cloudy] = 1;
  set_node_probability(bn, Rain, 0, parent_state, 0.2);
  set_node_probability(bn, Rain, 1, parent_state, 1 - 0.2);

  // WetGrass
  parent_state.clear();
  parent_state.add(Sprinkler, 1);
  parent_state.add(Rain, 1);

  parent_state[Sprinkler] = 0;
  parent_state[Rain]      = 0;
  set_node_probability(bn, Wet, 0, parent_state, 1.0);
  set_node_probability(bn, Wet, 1, parent_state, 1 - 1.0);

  parent_state[Sprinkler] = 1;
  parent_state[Rain]      = 0;
  set_node_probability(bn, Wet, 0, parent_state, 0.1);
  set_node_probability(bn, Wet, 1, parent_state, 1 - 0.1);

  parent_state[Sprinkler] = 0;
  parent_state[Rain]      = 1;
  set_node_probability(bn, Wet, 0, parent_state, 0.1);
  set_node_probability(bn, Wet, 1, parent_state, 1 - 0.1);

  parent_state[Sprinkler] = 1;
  parent_state[Rain]      = 1;
  set_node_probability(bn, Wet, 0, parent_state, 0.01);
  set_node_probability(bn, Wet, 1, parent_state, 1 - 0.01);

  using set_type = dlib::set<unsigned long>::compare_1b_c;
  using join_tree_type = graph<set_type, set_type>::kernel_1a_c;

  join_tree_type join_tree;

  create_moral_graph(bn, join_tree);
  create_join_tree(join_tree, join_tree);

  bayesian_network_join_tree solution(bn, join_tree);

  std::cout << "Using join tree algorithm:\n"
            << "p(Cloudy=0) = "
            << solution.probability(Cloudy)(0) << std::endl
            << "p(Cloudy=1) = "
            << solution.probability(Cloudy)(1) << std::endl
            << "p(Sprinkler=0) = "
            << solution.probability(Sprinkler)(0) << std::endl
            << "p(Sprinkler=1) = "
            << solution.probability(Sprinkler)(1) << std::endl
            << "p(Rain=0) = "
            << solution.probability(Rain)(0) << std::endl
            << "p(Rain=1) = "
            << solution.probability(Rain)(1) << std::endl
            << "p(Wet=0) = "
            << solution.probability(Wet)(0) << std::endl
            << "p(Wet=1) = "
            << solution.probability(Wet)(1) << std::endl;

  set_node_value(bn, Wet, 1);
  set_node_as_evidence(bn, Wet);
  bayesian_network_join_tree solution_evi(bn, join_tree);

  std::cout << "Using join tree algorithm:\n"
            << "P(Sprinkler = 1 | Wet = 1) = "
            << solution_evi.probability(Sprinkler)(1) << std::endl
            << "P(Rain = 1 | Wet = 1) = "
            << solution_evi.probability(Rain)(1) << std::endl;
}
catch (std::exception& e)
{
  std::cout << e.what() << std::endl;
}
